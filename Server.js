'use strict';
const fs = require('fs');
const net = require('net');
const fetch = require('node-fetch');
const Client = require('./Client'); // importing Client class
const moment = require('moment-timezone');
moment.tz.setDefault('Asia/Bangkok');
const UtilClass = require('./utils');
const utils = new UtilClass();
const cmdList = [ 'TK', 'TKQ', 'TKQ2', 'LK', 'UD', 'UD2', 'AL' ];
const unlimited = require('unlimited');
unlimited();

class Server {
	constructor(port, address) {
		this.port = port || 5000;
		this.address = address || '127.0.0.1';
		// Holds our currently connected clients
		this.clients = [];
	}
	/*
	 * Starting the server
	 * The callback argument is executed when the server finally inits
	*/

	start(callback) {
		let server = this; // we'll use 'this' inside the callback below
		setInterval(() => {
			server.connection.getConnections((err, count) => {
				console.log('NumberOfConnections : ', count);
			});
		}, 3000);
		server.connection = net.createServer((socket) => {
			let client = new Client(socket);
			let fullCommand = '';
			// fs.appendFileSync('./logs/test.log','OK \r\n')

			// socket.setTimeout(600000);
			// socket.on('timeout', () => {
			// 	console.log('socket timeout');
			// 	server.clients.splice(server.clients.indexOf(client), 1);
			// 	socket.destroy();
			// });

			// console.log("socket connected")

			let intervalHoldConnection = null;

			socket.on('data', (data) => {
				data = data.toString('binary');

				fullCommand = fullCommand.concat(data);
				fullCommand = this.amrPrimary(fullCommand, client);

				if (!client.id && !this.isForceClient(fullCommand)) {
					client.setDateTimeReq(moment().valueOf());
					client.setUserId(fullCommand);
					this.pushClientIfNotExist(client);
				}

				client.setDateTimeReq(moment().valueOf());
				if (data.indexOf(']') !== -1 && !this.isForceClient(fullCommand) && !this.isHttp(fullCommand)) {
					client.setCmd(fullCommand);
					if (client.ready) {
						let tmp = fullCommand;
						tmp = tmp.split(']');
						fullCommand = tmp[0] + ']';
						if (!client.data) {
							client.setData(fullCommand);
						}

						// if (client.id == '4700709713') {
						// console.log(client.id + ' FULLCMD ::: ' + fullCommand + ' ::: ' + moment().valueOf());
						console.log(client.id + ' CMD ::: ' + client.cmd);
						// }

						if (client.cmd == 'TK') {
							fs.writeFileSync('test.txt', fullCommand, 'binary');
							this.sendAMRBackToWatch(fullCommand, client);
							// client.ackAMR(true);
							// client.setIsAmr(true);
							// fs.writeFileSync('test.amr',new Buffer(client.getAmrFile(),'binary'))
							// console.log('TESTAMR LENGTH ',client.getAmrFile().length)

							let amr = utils.decryptAmr(new Buffer(client.getAmrFile(), 'binary'));
							let dir = './audio/' + client.id;

							if (!fs.existsSync(dir)) {
								fs.mkdirSync(dir);
							}
							let destination = dir + '/' + moment().valueOf() + '_' + client.id + '.amr';
							fs.writeFileSync(destination, new Buffer(amr, 'binary'));
							utils.convertTo(
								[ 'mp3', 'mp4' ],
								destination,
								null,
								() => {
									// fs.unlinkSync(destination)
								} /*,this.pushNoti(client.id)*/
							);
						} else {
							client.ack();
							// update location
							let locationList = [ 'UD', 'UD2', 'AL' ];
							if (locationList.indexOf(client.cmd) !== -1) {
								client.updateLocation(client.data);
							}

							let batteryList = [ 'LK' ];
							if (batteryList.indexOf(client.cmd) !== -1) {
								// console.log('LK CMD')
								client.uploadLK(client.data);
							}
						}

						if (!intervalHoldConnection) {
							intervalHoldConnection = setInterval(() => {
								if (client.id && !client.inProcess) {
									client.holdConnnection();
								}
							}, 30000);
						}

						client.setDateTimeRes(moment().valueOf());

						if (cmdList.indexOf(client.cmd) !== -1) client.log('SUCCESS');
						fullCommand = '';
						client.data = null;
						client.cmd = null;
					}
				}

				// else {
				// 	if (client.cmd == 'TK') {
				// 		// console.log('AMR')
				// 		client.ackAMR(true);
				// 		client.setIsAmr(true);
				// 	}
				// }
			});

			// Triggered when this client disconnects
			socket.on('end', () => {
				server.broadcast(fullCommand);
				console.log('connection close');
				console.log(`${client.id} disconnected.`);
				clearInterval(intervalHoldConnection);
				let remains = [];
				remains = this.clients.filter((clientData) => clientData.id != client.id);
				this.clients = remains;
				// server.clients.splice(server.clients.indexOf(client), 1);
				socket.destroy();
			});

			socket.on('error', (err) => {
				console.log(err);
				clearInterval(intervalHoldConnection);
				let remains = [];
				remains = this.clients.filter((clientData) => clientData.id != client.id);
				this.clients = remains;
				// server.clients.splice(server.clients.indexOf(client), 1);
				socket.destroy();
				// console.log("REMAIN :: ",server.clients.length)
			});
		});
		// starting the server
		this.connection.listen(this.port, this.address);
		// setuping the callback of the start function
		this.connection.on('listening', callback);
	}

	// TODO
	broadcast(fullCommand) {
		let server = this;

		if (this.isForceClient(fullCommand)) {
			console.log('BROADCAST COMMAND\n' + fullCommand);
			let forceClients = this.getClients(fullCommand);
			console.log('forceClients : ', forceClients.length);

			for (let i = 0; i < forceClients.length; i++) {
				let payload = forceClients[i].data;
				console.log('PAYLOAD : ' + payload);
				let forceClient = forceClients[i].client;
				let disconnect = forceClients[i].disconnect;
				forceClient.setInProcess(true);
				console.log('START SEND CMD ::: ' + forceClient.id + ' ::: DONE ' + moment().valueOf());
				forceClient.socket.write(payload, () => {
					console.log('END SEND CMD ::: ' + forceClient.id + ' ::: DONE ' + moment().valueOf());
					forceClient.setInProcess(false);
					if (disconnect) {
						for (let index = 0; index < server.clients.length; index++) {
							const id = server.clients[index].id;

							if (id == forceClient.id) {
								console.log(`FORCE CLOSE ${id} : ${forceClient.id}`);
								forceClient.socket.emit('end');
							}
						}
					}
				});
			}
		}
	}

	// make forceClients with data
	getClients(fullCommand) {
		let forceClients = [];
		let path = fullCommand.replace(/<server>(.*)?<\/server>/g, '').replace(/\n|\r\n/g, '');
		let cmdFromFile = fs.readFileSync(path, 'utf8');
		let gps_nos = this.getClientGpsNo(cmdFromFile);

		fullCommand = cmdFromFile.replace(/<server>(.*)?<\/server>/g, '').replace(/\n|\r\n/g, '');
		fullCommand = JSON.parse(fullCommand);
		fs.unlinkSync(path);

		if (gps_nos) {
			let client_gps_nos = gps_nos;
			for (let i = 0; i < client_gps_nos.length; i++) {
				for (let j = 0; j < this.clients.length; j++) {
					let client = this.clients[j];
					if (client.id == client_gps_nos[i]) {
						let cmd = fullCommand[client_gps_nos[i]];
						let disconnect = false;
						cmd = cmd.replace('{{command}}]', '');
						let payload = Buffer.from(fullCommand.data, 'base64');
						if (payload.indexOf('FACTORY') !== -1 || payload.indexOf('POWEROFF') !== -1) {
							disconnect = true;
						}
						let arr = [ Buffer.from(cmd), payload, Buffer.from(']') ];
						cmd = Buffer.concat(arr);
						forceClients.push({ client: client, data: cmd, disconnect: disconnect });
						break;
					}
				}
			}
		}
		return forceClients;
	}

	getClientGpsNo(str) {
		let gps_no = str.match(/<server>(.*)?<\/server>/g);
		if (gps_no) {
			gps_no = gps_no[0].replace('<server>', '').replace('</server>', '').split(',');
		} else {
			gps_no = null;
		}

		return gps_no;
	}

	isForceClient(str) {
		let match = str.match(/<server>(.*)?<\/server>/g);
		if (match) {
			match = true;
		} else {
			match = false;
		}

		return match;
	}

	pushNoti(id) {
		let query = {
			gps_no: id
		};
		return fetch(/*'http://safety2uproduction.saleafterservice.com/push_noti.php'*/ 'localhost:5001', {
			header: { 'Content-type': 'application/x-www-form-urlencoded' },
			method: 'POST',
			data: JSON.stringify(query)
		})
			.then((res) => res.json())
			.catch((error) => console.log(error));
	}

	amrPrimary(fullCommand, client) {
		if (fullCommand.indexOf('TK,#!AMR') !== -1) {
			client.ackAMR(true);
			client.setIsAmr(true);
			let cmds = fullCommand.split('[');
			for (let i = 0; i < cmds.length; i++) {
				if (cmds[i].indexOf('TK,#!AMR') !== -1) {
					fullCommand = '[' + cmds[i];
					console.log('::: AMR FILE :::');
					client.setData(fullCommand);
				}
			}
		}
		return fullCommand;
	}

	isHttp(data) {
		let http = false;
		if (data.includes('Host: safety2ubysas.com')) {
			console.log('HTTP INCOMING');
			http = true;
			this.socket.emit('end');
		}
		return http;
	}

	pushClientIfNotExist(newClient) {
		for (let client of this.clients) {
			if (client.id == newClient.id) {
				client.socket.emit('end');
				console.log(`client exist : ${client.address} : ${client.id}`);
				break;
			}
		}
		this.clients.push(newClient);
		console.log(`push client : ${newClient.id}`);
		console.log('\n\n:::::ALL CONNECTED CLIENT :::::');
		for (let i = 0; i < this.clients.length; i++) {
			console.log(this.clients[i].id);
		}
		console.log(':::::END CONNECTED CLIENT :::::');
	}

	sendAMRBackToWatch(fullCommand, client) {
		console.log('SEND AMR BACK TO WATCH');
		let payload = Buffer.from(Buffer.from(fullCommand, 'binary').toString('base64'), 'base64');
		client.socket.write(payload);
	}
}

module.exports = Server;
