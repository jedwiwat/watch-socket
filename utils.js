'use strict';
var _bin2hex = [
    '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
];

var _hex2bin = [
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, // 0-9
     0,10,11,12,13,14,15, 0, 0, 0, 0, 0, 0, 0, 0, 0, // A-F
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0,10,11,12,13,14,15, 0, 0, 0, 0, 0, 0, 0, 0, 0, // a-f
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
];

const fs = require('fs')
const moment = require('moment-timezone');

class utils {
	constructor(){

	}

	bin2hex(str) {
	    var len = str.length;
	    var rv = '';
	    var i = 0;
	    var c;
	    
	    while (len-- > 0) {
	        c = str.charCodeAt(i++);

	        rv += _bin2hex[(c & 0xf0) >> 4];
	        rv += _bin2hex[(c & 0x0f)];
	    }

	    return rv;
	}

	hex2bin(str) {
	    var len = str.length;
	    var rv = '';
	    var i = 0;

	    var h1,h2;
	    var c1;
	    var c2;

	    while (len > 1) {
	        h1 = str.charAt(i++);
	        c1 = h1.charCodeAt(0);
	        h2 = str.charAt(i++);
	        c2 = h2.charCodeAt(0);
	        
	        rv += String.fromCharCode((_hex2bin[c1] << 4) + _hex2bin[c2]);
	        len -= 2;
	    }

	    return rv;
	}

	decryptAmr(amr){
		amr = amr.toString('hex')
		let keys = {'7d01' : '7d','7d02' : '5b','7d03' : '5d','7d04' : '2c','7d05' : '2a'}
		let isDone = false
		while(!isDone){
			let isFound = false;
			for(let key in keys){
				
				if(amr.indexOf(key) !== -1){
					amr = amr.replace(new RegExp(key,'g'),keys[key])
					isFound = true
				}
			}

			if(!isFound) {
				isDone = true
			}

		} 
		
		return this.hex2bin(amr)
	}

	encryptAmr(amr){
		amr = amr.toString('hex')
		let keys = {'7d01' : '7d','7d02' : '5b','7d03' : '5d','7d04' : '2c','7d05' : '2a'}
		let isDone = false
		amr = this.chunk(amr,2)

		for (var i = 0; i < amr.length; i++) {
			for(let key in keys){
				
				if(amr[i] == keys[key]){
					amr[i] = key
				}
			}
			
		}
		
		amr = amr.join('')
		return this.hex2bin(amr)
	}

	// mp3 mp4 amr
	convertTo(ext,amrPath,copyPath,callback){
		// console.log("TEMP :: "+amrPath)
		var shell = require('shelljs');
		let commands = []
		let promises = []


		for (var i = 0; i < ext.length; i++) {
			let newPath = amrPath
			if(copyPath){
				newPath = copyPath
			}
			let mp3Path = newPath.split('.')
			mp3Path[mp3Path.length-1] = ext[i]
			mp3Path = mp3Path.join('.')

			console.log("PATH :: "+mp3Path)
			var cmd = `ffmpeg -i ${amrPath} -strict -2 ${mp3Path} >/dev/null 2>&1 `
			if(ext[i] == 'amr-nb'){
				cmd = `sox ${amrPath} ${mp3Path} >/dev/null 2>&1`
			}

			promises.push(new Promise(function(resolve, reject) {
				if(shell.exec(cmd).code === 0){
					resolve("OK")
				}
				else{
					resolve("OK")
					console.log("ERROR CONVERT "+ext[i])
				}
			}))
		}

		return Promise.all(promises).then((values)=>{
			if(callback)
				callback()
		})

	}


	

	getDataLengthHex(data){
		return data.length.toString(16).padStart(4,'0').toUpperCase()
	}

	chunk(str,number){
		var chunks = [];
		for (var i = 0, charsLength = str.length; i < charsLength; i += number) {
		    chunks.push(str.substring(i, i + number));
		}
		return chunks
	}

	stringToHex(strs){
		strs = strs.split('')

		for (var i = 0; i < strs.length; i++) {
			const buf = Buffer.from(strs[i], 'ascii')
			strs[i] = buf.toString('hex').padStart('4',0)
		}
		return strs.join('')

	}

}
module.exports = utils;