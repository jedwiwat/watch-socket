'use strict';
const fs = require('fs');
const fetch = require('node-fetch');
const utilClass = require('./utils');
const utils = new utilClass();
const moment = require('moment-timezone');
moment().tz('Asia/Bangkok');
const cmdList = [ 'TK', 'TKQ', 'TKQ2', 'LK', 'UD', 'UD2', 'AL' ];

class Client {
	constructor(socket) {
		this.ready = false;
		this.address = socket.remoteAddress;
		this.port = socket.remotePort;
		this.name = `${this.address}:${this.port}`;
		this.id = null;
		this.cmd = null;
		this.data = null;
		this.dataLength = 0;
		this.socket = socket;
		this.datetime_req = null;
		this.datetime_res = null;
		this.isAmr = false;
		this.session = null;
		this.inProcess = false;
		this.recvCommand = null;
		this.initInterval = false;
	}
	setInProcess(inProcess) {
		this.inProcess = inProcess;
	}
	getSessionId() {
		return this.session;
	}
	setIsAmr(isAmr) {
		this.isAmr = isAmr;
	}

	getAmrFile() {
		let file = this.data.split(',').slice(1).join('');
		return file;
	}

	setUserId(data) {
		let datas = data.split(']')[0].split('*');
		this.id = datas[1];
		this.session = this.id + '_' + this.datetime_req + parseInt(Math.random() * 1000);
	}

	getUserId() {
		return this.id;
	}

	setCmd(data) {
		let cmd = '';
		if (data) {
			if (data.indexOf('*TK,#!AMR') !== -1) {
				cmd = 'TK';
			} else if (data.indexOf('*UD2') !== -1) {
				cmd = 'UD2';
			} else if (data.indexOf('*UD') !== -1) {
				cmd = 'UD';
			} else if (data.indexOf('*LK') !== -1) {
				cmd = 'LK';
				this.ready = true;
			} else if (data.indexOf('*AL') !== -1) {
				cmd = 'AL';
			} else if (data.indexOf('*TKQ2') !== -1) {
				cmd = 'TKQ2';
			} else if (data.indexOf('*TKQ') !== -1) {
				cmd = 'TKQ';
			} else if (this.cmd == 'TK') {
				cmd = 'TK';
			}
		}
		this.cmd = cmd;
	}

	getCmd() {
		return this.cmd;
	}

	setDateTimeReq(datetime) {
		this.datetime_req = datetime;
	}

	setDateTimeRes(datetime) {
		this.datetime_res = datetime;
	}

	setData(command) {
		if (command) {
			command = command.replace('[', '').replace(']', '').split('*');
			this.data = command[3];
		} else {
			this.data = null;
		}
	}

	getData(data) {
		return this.data;
	}

	setDataLength(data) {
		this.dataLength = data.length;
	}

	getDataLength(data) {
		return this.dataLength;
	}

	getFullCmd() {
		let cmd = '';
		try {
			cmd = `[3G*${this.id}*${this.data.length.toString(16).padStart(4, '0')}*${this.data}]`;
		} catch (e) {
			cmd = 'ERROR';
		}
		return cmd;
	}

	updateLocation() {
		if (this.data) {
			let datas = this.data.split(',').splice(1);

			if (datas.length > 1) {
				let query = {
					gps_no: this.id,
					date: datas[0],
					time: datas[1],
					locate: datas[2],

					gps_lat: datas[3],
					lat_identify: datas[4],
					gps_long: datas[5],
					long_identify: datas[6],
					speed: datas[7],
					direction: datas[8],
					altitude: datas[9],
					number_of_satellite: datas[10],
					gsm_signal: datas[11],
					battery: datas[12],
					steps: datas[13],
					sleep_roll: datas[14],
					watch_status: datas[15],
					number_of_station: datas[16],
					connect_to_station: datas[17]
				};

				let numberOfStation = datas[16];
				let wifiNumberIndex = 20 + numberOfStation * 3;
				let numberOfWifi = datas[wifiNumberIndex];

				// newDatas[0] = datas[20]
				let newDatas = datas.slice(20);
				let i = 1;

				if (numberOfStation > 0) {
					i = 1;
					let stations = [];
					let obj = {};
					while (i <= numberOfStation * 3) {
						if (i % 3 == 1) {
							obj.station = newDatas[i - 1];
						} else if (i % 3 == 2) {
							obj.code = newDatas[i - 1];
						} else if (i % 3 == 0) {
							obj.signal = newDatas[i - 1];
							stations.push(obj);
							obj = {};
						}
						i++;
					}
					query.number_of_station = numberOfStation;
					query.stations = stations;
				}

				if (numberOfWifi > 0) {
					newDatas = datas.slice(wifiNumberIndex + 1);
					i = 1;
					let wifis = [];
					let obj = {};
					while (i <= numberOfWifi * 3) {
						if (i % 3 == 1) {
							obj.name = newDatas[i - 1];
						} else if (i % 3 == 2) {
							obj.address = newDatas[i - 1];
						} else if (i % 3 == 0) {
							obj.signal = newDatas[i - 1];
							wifis.push(obj);
							obj = {};
						}
						i++;
					}
					query.wifis = wifis;
					query.number_of_wifi = numberOfWifi;
				}

				// console.log("Location Data :: ",query)
				return fetch('http://safety2uproduction.saleafterservice.com/update_location.php', {
					headers: {
						'Content-Type': 'application/json; charset=utf-8'
						// "Content-Type": "application/x-www-form-urlencoded",
					},
					method: 'POST',
					body: JSON.stringify(query)
				})
					.then((resp) => resp.json())
					.then(function(data) {
						// console.log(data)
					})
					.catch((error) => console.log(error));
			}
		}
	}

	uploadLK() {
		if (this.data) {
			let datas = this.data.split(',').splice(1);
			if (datas.length > 1) {
				console.log('UPLOAD BATTERY');
				let query = {
					gps_no: this.id,
					battery: datas[2]
				};
				// console.log(query)
				return fetch('http://safety2uproduction.saleafterservice.com/upload_lk.php', {
					headers: {
						'Content-Type': 'application/json; charset=utf-8'
						// "Content-Type": "application/x-www-form-urlencoded",
					},
					method: 'POST',
					body: JSON.stringify(query)
				})
					.then((resp) => resp.json())
					.then(function(data) {
						// console.log(data)
					})
					.catch((error) => console.log(error));
			}
		}
	}

	ackAMR(isFin) {
		let cmd = `[3G*${this.id}*0004*TK,1]`;
		if (!isFin) {
			cmd = `[3G*${this.id}*0004*TK,0]`;
		}
		console.log(cmd);
		this.socket.write(cmd);
	}

	ack() {
		if (cmdList.indexOf(this.cmd) !== -1) {
			let cmd = `[3G*${this.id}*${this.cmd.length.toString(16).padStart(4, '0')}*${this.cmd}]`;
			this.socket.write(cmd);
		}
	}

	holdConnnection() {
		let cmd = `[3G*${this.id}*0002*LK]`;
		this.socket.write(cmd);
	}

	log(desc = null) {
		this.summaryLog(desc);
		this.detailLog(desc);
	}

	summaryLog(desc) {
		let DATETIME_REQ = moment(this.datetime_req);
		let DATETIME_RES = moment(this.datetime_res);
		let PROCESSING_TIME = DATETIME_RES.diff(DATETIME_REQ);

		DATETIME_REQ = DATETIME_REQ.format('YYYYMMDD:HHmmss');
		DATETIME_RES = DATETIME_RES.format('YYYYMMDD:HHmmss');

		let SESSION = this.session;
		let CMD = this.cmd;
		let ID = this.id;
		let RESULT_CODE = null;
		let RESULT_DESC = desc;
		fs.appendFileSync(
			'./logs/summary.log',
			`${DATETIME_REQ}|${SESSION}|${ID}|${CMD}|${DATETIME_RES}|${RESULT_CODE}|${RESULT_DESC}|${PROCESSING_TIME}\r\n`
		);
	}

	detailLog(desc) {
		let DATETIME_REQ = moment(this.datetime_req);
		let DATETIME_RES = moment(this.datetime_res);
		let PROCESSING_TIME = DATETIME_RES.diff(DATETIME_REQ);

		DATETIME_REQ = DATETIME_REQ.format('YYYYMMDD:HHmmss');
		DATETIME_RES = DATETIME_RES.format('YYYYMMDD:HHmmss');

		let SESSION = this.session;
		let CMD = this.cmd;
		let ID = this.id;
		let RESULT_CODE = null;
		let RESULT_DESC = desc;
		fs.appendFileSync(
			'./logs/detail.log',
			`${DATETIME_REQ}|${SESSION}|${ID}|${CMD}|${DATETIME_RES}|${RESULT_CODE}|${RESULT_DESC}|${PROCESSING_TIME}\r\n` +
				`DETAIL: ${this.isAmr ? 'AMR FILE' : this.getFullCmd()}\r\n`
		);
	}

	setInitInterval(init) {
		this.initInterval = init;
	}
}
module.exports = Client;
