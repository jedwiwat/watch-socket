'use strict';
// const cluster = require('cluster');
// const numCPUs = require('os').cpus().length;

// importing Server class
const Server = require('./Server');
// Our configuration
const PORT = 5000;
const ADDRESS = "0.0.0.0"

var server = new Server(PORT, ADDRESS);


// if (cluster.isMaster) {
// 	// Fork workers.
// 	for (var i = 0; i < numCPUs; i++) {
// 		cluster.fork();
// 	}

// 	cluster.on('exit', (worker, code, signal) => {
// 		console.log(`worker ${worker.process.pid} died`);
// 	});
// } 
// else 
// {
    server.start(() => {
	  console.log(`Server started at: ${ADDRESS}:${PORT}`);
	});
// }

 //  server.start(() => {
	//   console.log(`Server started at: ${ADDRESS}:${PORT}`);
	// });


// require('./api');

