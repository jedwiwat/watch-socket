#!/bin/sh
sudo apt-get update
mkdir dependencies
cd dependencies

curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt-get install nodejs
sudo apt-get install build-essential
sudo apt-get update
sudo npm install -g pm2

# install sox
sudo apt-get install sox libsox-fmt-all

#install ffmpeg
sudo add-apt-repository ppa:djcj/hybrid
sudo apt-get update
sudo apt-get install ffmpeg

# install app dependencies
cd app
sudo npm install
