const express = require('express')
const app = express()
const UtilClass = require('./utils')
const utils = new UtilClass()
const fs = require('fs')
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser')
const net = require('net')
const shell = require('shelljs');


const moment = require('moment-timezone');
moment.tz.setDefault('Asia/Bangkok')

const uuidv4 = require('uuid/v4');
app.set('host','http://safety2ubysas.com:5001');

app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
app.use(fileUpload());

app.get('/', (req, res) => res.send('Hello World!'))



// app.get('/test2',(req,res)=>{
// 	let data = fs.readFileSync('./file/canplay.amr')
// 	data = utils.encryptAmr(data)
// 	fs.writeFileSync('./file/test_encrypt.amr',data,'binary')
// 	res.end('OK')
// })



app.post('/send-command', async (req, res)=>{


	let username = req.body.username || null
 	let ids = req.body.gps_no || null
	let command = req.body.command || null
	
	// console.log("COMMAND : ",command);

	let isAmr = false
	if(ids){
		ids = ids.split(',')
	}
	else{
		return res.status(500).send("gps_no is required.")
	}
	
	let results = []
	
	if(req.files && req.files.file){
		if (!username){
	    	return res.status(500).send("username is required.")
	    }

		let audioFile = req.files.file;
		let extension = audioFile.name.split('.')
		extension = extension[extension.length-1]
		let filename = moment().valueOf()

		let tmpName = username+"_"+filename+'.'+extension
		let filePath = './audio/tmp/'+tmpName

		let mp3FilePath = './audio/tmp/tmp_'+username+"_"+filename+'.mp3'

		let idsPromises = []
		
		audioFile.mv(filePath, function(err) {
		    if (err){
		    	return res.status(500).send(err)
		    }

		    // convertToMp3
			utils.convertTo(['mp3'],filePath,mp3FilePath)
		    .then(()=>{
		    	let insertRedis = false

		    	let redisKeys = {}

				for (let i = 0; i < ids.length; i++) {

					let id = ids[i]

					let fullcmd = ''

					let watchdir = './audio/'+id

					let userDir = watchdir+'/'+username
					let file_id = filename+'_'+uuidv4()
					let audioUserPath = userDir+'/'+file_id+'.xxx'

					let amrPath = userDir+'/'+file_id+'.amr-nb'

					if (!fs.existsSync(watchdir)){
						fs.mkdirSync(watchdir,0777)
					}

					if (!fs.existsSync(userDir)){
						fs.mkdirSync(userDir,0777,false)
					}

					let amrFile = null

					idsPromises.push(new Promise(function(resolve, reject) {
						utils.convertTo(['amr-nb','mp3','mp4'],mp3FilePath,audioUserPath)
						.then((res)=>{
							if (fs.existsSync(amrPath)){
								if(!insertRedis){
									insertRedis = true
									amrFile = fs.readFileSync(amrPath)
									// console.log("AMR FILE ::"+amrFile)
									command = 'TK,'.concat(utils.encryptAmr(amrFile))

									for (let j = 0; j < ids.length; j++) {

										fullcmd = `[3G*${ids[j]}*${utils.getDataLengthHex(command)}*{{command}}]`

										let redisKey = ids[j]
										// console.log(command)

										redisKeys[redisKey] = fullcmd
										redisKeys.data = Buffer.from(command,'binary').toString('base64')

									}

								}

								results.push({
									success:id
								})
								resolve("OK")
							}
						})
					})
					)

				}

				Promise.all(idsPromises).then(()=>{
						// broadcast(ids,redisKeys,true)
						broadcastFilePath(ids,redisKeys,true);
						fs.unlinkSync(filePath);
						fs.unlinkSync(mp3FilePath);
					return res.send(results).status(200)
				})

			})
			

		})
	}
	else{
		if(!command) return res.status(500).send("command is required.")

		let redisKeys = {}
		for (var i = 0; i < ids.length; i++) {
			let id = ids[i]

			let fullcmd = ''

			fullcmd = `[3G*${id}*${utils.getDataLengthHex(command)}*{{command}}]`
			
			results.push({
				success:id
			})
			// console.log(command);
			
		
			let redisKey = ids[i]

			redisKeys[redisKey] = fullcmd
			redisKeys.data = Buffer.from(command.toUpperCase(),'binary').toString('base64')
			
		}
		// broadcast(ids,redisKeys)
		broadcastFilePath(ids,redisKeys)
		return res.send(results).status(200)

	}

})

app.post('/getFile',function(req,res){
	res.setHeader('Content-Type', 'application/json');

	let platform = req.body.platform || 'android'
	let gps_no = req.body.gps_no || null
	let username = req.body.username || null

	let filePathList = []
	let fileType = 'mp3'
	let result = {}					
	if(gps_no && username){
		let watchDir = './audio/'+gps_no+'/'

		let dir = './audio/'+gps_no+'/'+username+'/'
		let host = app.get('host')+'/audio/'+gps_no+'/'+username+'/'
		let watchHost = app.get('host')+'/audio/'+gps_no+'/'
		console.log("CHECK DIR : ",dir)
		if (fs.existsSync(watchDir)){
		    if(platform.toLowerCase() === 'ios'){
				fileType = 'mp4'
			}
			else{
				fileType = 'mp3'
			}
			console.log("READ DIR")
			fs.readdir(watchDir, (err, fileWatches) => {	
				console.log("WATCHES :",fileWatches)
				fs.readdir(dir, (err, files) => {	

					if(fileWatches && fileWatches.length > 0){
						let objs = []
						fileWatches.forEach(file => {
				    		if(file.endsWith(fileType)){

				    			let filePath = watchHost+file

				    			filePathList.push({
				    				time:moment.unix(file.split('_')[0]/1000).unix()+"",
				    				type:'watch',
				    				file_location:filePath
				    			})

				    		}
			  			})
					}

					if(files && files.length > 0){
						let objs = []
						files.forEach(file => {
				    		if(file.endsWith(fileType)){

				    			let filePath = host+file

				    			filePathList.push({
				    				time:moment.unix(file.split('_')[0]/1000).unix()+"",
				    				type:'mobile',
				    				file_location:filePath

				    			})

				    		}
			  			})
					}
					if(filePathList.length == 0){
						let filePath = app.get('host')+'/audio/1/default/1536113095955_e012b834-d165-4794-9ed4-35ce2f520686.'+fileType;
						filePathList.push({
		    				time:"0",
		    				type:'mobile',
		    				file_location:filePath

		    			})
					}	
					result = {
						Success:filePathList
					}
					return res.send(result).status(200)
					
				})
			})
		}
		else{
			if(filePathList.length == 0){

				let filePath = app.get('host')+'/audio/1/default/1536113095955_e012b834-d165-4794-9ed4-35ce2f520686.'+fileType;
				filePathList.push({
    				time:"0",
    				type:'mobile',
    				file_location:filePath

    			})
			}	
			result = {
					Success:filePathList
				}
			return res.send(result).status(200)
		}
	}else{
		return res.send('username and gps_no is required.').status(500)
	}
	
})

app.use('/audio', express.static('audio'))
app.use('/logs', express.static('../.pm2/logs'))

app.get('/flush',function (req,res){
	shell.exec('pm2 flush')
	res.send("OK").status(200)
})

app.listen(5001, () => console.log('Example app listening on port 5001!'))


function broadcast(ids,redisKeys,isAmr = false){

	console.log("BROADCAST ")
	let client = new net.Socket();
	client.connect(5000, 'safety2ubysas.com', function() {
		console.log('Connected');
		let cmd = `<server>${ids.join(',')}</server>${JSON.stringify(redisKeys)}`
		client.write(cmd);
		client.destroy();
	});
}

function broadcastFilePath(ids,redisKeys){

	console.log("BROADCAST ")
	let data = `<server>${ids.join(',')}</server>${JSON.stringify(redisKeys)}`
	let filePath = './command/'+uuidv4();
	fs.writeFileSync(filePath,data,'binary');

	let cmd = `<server>${ids.join(',')}</server>${filePath}`;
	let client = new net.Socket();
	client.connect(5000, 'safety2ubysas.com', function() {
		console.log('Connected');
		console.log("CMD LENGTH :",cmd.length);
		client.write(cmd);
		client.destroy();
	});
}